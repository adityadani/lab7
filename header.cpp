#include "header.h"
#include "routing_table.h"

extern routing_table_entry_st routing_table_entry[];
extern int rt_table_entry;
extern int create_icmp_packet(struct icmphdr * , struct icmphdr *, struct iphdr *, int, int);
extern int create_icmph(struct icmphdr * , struct icmphdr *, struct iphdr *, int, int);

void get_ip_from_int32(uint32_t ip, char *ip_str) {
	unsigned char octet[4]  = {0,0,0,0};
	for (int i=0; i<4; i++)
		{
			octet[i] = ( ip >> (i*8) ) & 0xFF;
		}
	sprintf(ip_str, "%d.%d.%d.%d", octet[0], octet[1], octet[2], octet[3]);
	//printf("IP is : %d.%d.%d.%d\n", octet[0], octet[1], octet[2], octet[3]);
}

int check_my_packet(const u_char *pkt, char *intf) {
	struct ether_header *eh;
	char *interface;
	struct ifreq if_mac;
	int flag = 0;

	eh = (struct ether_header *)pkt;
	get_mac_address(&if_mac, intf);
	for(int i=0;i<6;i++) {
		if((u_int8_t)(if_mac.ifr_hwaddr.sa_data)[i] != eh->ether_dhost[i]) {
			flag = 1;
			break;
		}
	}
	return flag;
}

char * get_interface(int entry) {
	return routing_table_entry[entry].interface;
}

struct iphdr * change_ip_header(struct iphdr * iph) {
	char ip[20];
	iph->ttl--;
	if(iph->ttl == 0) {
		
		//Need to do something here. Like send ICMP pkt 
		// host not found.
	}
	
	/* Calculate IP checksum on completed header */
	iph->check = 0;
	iph->check = csum((unsigned short *)(iph), sizeof(struct iphdr)/2);


	//printf("\nSource IP : ");
	get_ip_from_int32(iph->saddr, ip);
	//printf("\nDestination IP : ");
	get_ip_from_int32(iph->daddr, ip);

	/*	iph->ihl = 5;
	iph->version = 4;
	iph->tos = 16; // Low delay
	iph->id = htons(54321);
	iph->ttl = 5; //  keeping dummy value as 5 hops --> Change this value...Decreament it.
	iph->protocol = 17; // UDP
	// Source IP Addr
	iph->saddr = inet_addr(source_ip);
	 Destination IP address 
	iph->daddr = inet_addr(dest_ip);
	iph->check = 0;
	tx_len += sizeof(struct iphdr);
	*/

	return iph;
}


void get_mac_address(struct ifreq * if_mac, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if_mac->ifr_addr.sa_family = AF_INET;
	memset(if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFHWADDR, if_mac) < 0)
		perror("SIOCGIFHWADDR");
	close(fd);
}

void get_ip_address(struct ifreq *if_idx, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(if_idx, 0, sizeof(struct ifreq));
	if_idx->ifr_addr.sa_family = AF_INET;
	strncpy(if_idx->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFADDR, if_idx) < 0)
		perror("SIOCGIFADDR");
	printf("\n Interface : %s , ifr_name : %s", interface, if_idx->ifr_name);
	close(fd);
}

void get_ip_address_index(struct ifreq *if_idx, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(if_idx, 0, sizeof(struct ifreq));
	if_idx->ifr_addr.sa_family = AF_INET;
	strncpy(if_idx->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFINDEX, if_idx) < 0)
		perror("SIOCGIFINDEX");
	close(fd);	
}


char * change_ethernet_header(struct ether_header *eh, int entry) {
	
	/* Ethernet header */
	eh->ether_shost[0] = routing_table_entry[entry].interfaceMAC[0];
	eh->ether_shost[1] = routing_table_entry[entry].interfaceMAC[1];
	eh->ether_shost[2] = routing_table_entry[entry].interfaceMAC[2];
	eh->ether_shost[3] = routing_table_entry[entry].interfaceMAC[3];
	eh->ether_shost[4] = routing_table_entry[entry].interfaceMAC[4];
	eh->ether_shost[5] = routing_table_entry[entry].interfaceMAC[5];
	//printf("\n Source MAC : %x:%x:%x:%x:%x:%x", eh->ether_shost[0], eh->ether_shost[1], 
	//eh->ether_shost[2], eh->ether_shost[3], eh->ether_shost[4], eh->ether_shost[5]);

	//The next destination here should be found out by lookup on the routing
	//table.

	eh->ether_dhost[0] = routing_table_entry[entry].MAC_next_hop[0];
	eh->ether_dhost[1] = routing_table_entry[entry].MAC_next_hop[1];
	eh->ether_dhost[2] = routing_table_entry[entry].MAC_next_hop[2];
	eh->ether_dhost[3] = routing_table_entry[entry].MAC_next_hop[3];
	eh->ether_dhost[4] = routing_table_entry[entry].MAC_next_hop[4];
	eh->ether_dhost[5] = routing_table_entry[entry].MAC_next_hop[5];
	//printf("\n Dest MAC : %x:%x:%x:%x:%x:%x", eh->ether_dhost[0], eh->ether_dhost[1], 
	//eh->ether_dhost[2], eh->ether_dhost[3], eh->ether_dhost[4], eh->ether_dhost[5]);

	eh->ether_type = htons(ETH_P_IP);
	tx_len += sizeof(struct ether_header);
	return routing_table_entry[entry].interface;
}


struct udphdr * change_udp_header(struct udphdr *udph) {
	/* UDP Header */
	udph->source = htons(3423);
	udph->dest = htons(5342);
	udph->check = 0; // skip
	tx_len += sizeof(struct udphdr);
	/* Packet data */
	strncpy(sendbuf + tx_len, "Ajinkya Ghodke Aditya Dani", strlen("Ajinkya Ghodke Aditya Dani"));
	tx_len += strlen("Ajinkya Ghodke Aditya Dani");
	sendbuf[tx_len++] = 'A';
	sendbuf[tx_len++] = 'D';
	sendbuf[tx_len++] = 'A';
	sendbuf[tx_len++] = 'J';
	return udph;
}

unsigned short csum(unsigned short *buf, int nwords)
{
    unsigned long sum;
    for(sum=0; nwords>0; nwords--)
        sum += *buf++;
    sum = (sum >> 16) + (sum &0xffff);
    sum += (sum >> 16);
    return (unsigned short)(~sum);
}


