#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "packet_queue.h"

extern int build_routing_table();

std::queue<QueueEntry> pkt_queue;

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
	QueueEntry entry;
	
	memcpy(&entry.hdr, header, sizeof(struct pcap_pkthdr));
	entry.pkt = (char *)malloc(header->caplen);
	memcpy(entry.pkt, packet, header->caplen);
	pkt_queue.push(entry);
	printf("\nGot the packet with len : %d", header->caplen);
	return;
}


int main(int argc, char *argv[])
{
	pcap_t *handle;			
	char dev[20], errbuf[PCAP_ERRBUF_SIZE];	
	struct bpf_program fp;		
	bpf_u_int32 mask;
	bpf_u_int32 net;		
	struct pcap_pkthdr header;	
	const u_char *packet;		
	int num_packets=10;

	build_routing_table();
	
	strcpy(dev,"eth1");
	if (dev == NULL) {
		fprintf(stderr, "error in finding default device: %s\n", errbuf);
		return(2);
	}
	
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "error in getting netmask for device %s: %s\n", dev, errbuf);
		net = 0;
		mask = 0;
	}
	
	handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "error in opening the device %s: %s\n", dev, errbuf);
		return(2);
	}
	
	/* Grab a packet */
	pcap_loop(handle, num_packets, got_packet, NULL);
	pcap_close(handle);
	send_on_raw(argv);
	return(0);
}
