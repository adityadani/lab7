# include <netinet/ip_icmp.h>
# include <netinet/ip.h>
# include <netinet/if_ether.h>
# include <string.h>


extern unsigned short csum(unsigned short*, int); 
int icmp_len = 0;


void copy_MAC( struct ether_header *eth1, struct ether_header * eth2){

	for (int i =0 ; i<6 ; i++){
 		eth2->ether_shost[i]= eth1->ether_dhost[i];
 		eth2->ether_dhost[i]= eth1->ether_shost[i];
	}
}


void create_eth( struct ether_header *eh_i, struct ether_header *eh_o) {
	copy_MAC(eh_i, eh_o);
	eh_o->ether_type = htons(ETHERTYPE_IP);
        icmp_len += sizeof(struct ether_header);
}

void create_icmph( struct icmphdr *icmph_i, struct icmphdr *icmph_o, struct iphdr *iph_i, int type, int code) {


	char *ptr = (char*) icmph_o;
	
	icmph_o->type = type;
	icmph_o->code = code;                     // TTL count exceeded
	icmph_o->un.echo.id = icmph_i->un.echo.id; 	
	icmph_o->un.echo.sequence = 1;
			
	ptr += sizeof(struct icmphdr);	
	memcpy(ptr,iph_i,28); 	
	icmp_len += (sizeof(struct icmphdr)+28);

	icmph_o->checksum = 0;
	icmph_o->checksum = csum((unsigned short *)(icmph_o), (sizeof(struct icmphdr)/2)+ 14); //can be wrong !!! 

}


void create_iph( struct iphdr *iph_i, struct iphdr *iph_o){

	
	iph_o->saddr = iph_i->daddr;
	iph_o->daddr = iph_i->saddr;
	iph_o->ihl = 5;                     // need to change to 20
	iph_o->tot_len = htons(56);
        iph_o->version = 4;
        iph_o->tos = 16;			 // Low delay
        iph_o->id = htons(54321);
        iph_o->ttl = 64; 			
        iph_o->protocol = IPPROTO_ICMP; 			
        iph_o->check = 0;
	iph_o->check = csum((unsigned short *)(iph_o), sizeof(struct iphdr)/2);
        icmp_len += sizeof(struct iphdr);
		

}



int create_icmp_packet(char *incoming_packet , char *outgoing_packet , int type , int code) {



	struct ether_header *eh_i, *eh_o;
	struct iphdr *iph_o , *iph_i;

	struct icmphdr *icmph_i , *icmph_o;

	memset(outgoing_packet, 0, 65507);
	eh_i = (struct ether_header*) incoming_packet;
	iph_i = (struct iphdr*) (incoming_packet + sizeof(struct ether_header));
	icmph_i = (struct icmphdr*) (incoming_packet + sizeof(struct ether_header) + sizeof (struct iphdr));


	eh_o = (struct ether_header*) outgoing_packet;
	iph_o = (struct iphdr*) (outgoing_packet + sizeof(struct ether_header));
	icmph_o = (struct icmphdr*) (outgoing_packet + sizeof(struct ether_header) + sizeof (struct iphdr));
	
	create_eth( eh_i, eh_o);
	create_iph( iph_i, iph_o);
	create_icmph( icmph_i, icmph_o,iph_i, type, code);

	int ret = icmp_len;
	icmp_len = 0;
	return ret;	

}



