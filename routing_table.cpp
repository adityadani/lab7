#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/ether.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include "header.h"
#include "routing_table.h"

int rt_table_entry = 0; // Keeps track of entries in the routing table.

routing_table_entry_st routing_table_entry[20];
arpFileContents arpContents[20];

int arp_entry = 0;

int find_entry_arp_contents(char *ip) {
	for(int i=0;i<arp_entry;i++) {
		if(strcmp(ip, arpContents[i].ip) == 0)
			return i;
	}
	return -1;
}

int check_arp_for_destination_unreachable(char *ip) {
	for(int i=0;i<arp_entry;i++) {
		if(strcmp(ip, arpContents[i].ip) == 0 && strcmp(arpContents[i].mac, "000000000000") != 0)
			return i;
	}
	return -1;
	
}
   

void get_mac(struct ifreq * if_mac, const char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if_mac->ifr_addr.sa_family = AF_INET;
	memset(if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFHWADDR, if_mac) < 0)
		perror("SIOCGIFHWADDR");
	close(fd);
}

void print_routing_table() {
	for (int i=0 ; i<rt_table_entry ; i++ ) {
		printf("\nEntry %d", i+1);
		printf("%s \t %s \t %x:%x:%x:%x:%x:%x \t %s \t %x:%x:%x:%x:%x:%x", routing_table_entry[i].subnet, 
		       routing_table_entry[i].IP_next_hop, routing_table_entry[i].MAC_next_hop[0], routing_table_entry[i].MAC_next_hop[1],
		       routing_table_entry[i].MAC_next_hop[2], routing_table_entry[i].MAC_next_hop[3], routing_table_entry[i].MAC_next_hop[4],
		       routing_table_entry[i].MAC_next_hop[5], routing_table_entry[i].interface, routing_table_entry[i].interfaceMAC[0], 
		       routing_table_entry[i].interfaceMAC[1], routing_table_entry[i].interfaceMAC[2], routing_table_entry[i].interfaceMAC[3],
		       routing_table_entry[i].interfaceMAC[4], routing_table_entry[i].interfaceMAC[5]);
	}
}

void insert_rt (const char* subnet, const char* IP_next_hop, const char* hwa, const char*  interface) {
        
	struct ifreq if_req;

	strcpy(routing_table_entry[rt_table_entry].subnet, subnet);
        strcpy(routing_table_entry[rt_table_entry].IP_next_hop , IP_next_hop);
	sscanf(hwa, "%x:%x:%x:%x:%x:%x",&(routing_table_entry[rt_table_entry].MAC_next_hop[0]), 
	       &(routing_table_entry[rt_table_entry].MAC_next_hop[1]), 
	       &(routing_table_entry[rt_table_entry].MAC_next_hop[2]), 
	       &(routing_table_entry[rt_table_entry].MAC_next_hop[3]), 
	       &(routing_table_entry[rt_table_entry].MAC_next_hop[4]), 
	       &(routing_table_entry[rt_table_entry].MAC_next_hop[5]));
        strcpy(routing_table_entry[rt_table_entry].interface , interface);
	get_mac(&if_req, interface);
	routing_table_entry[rt_table_entry].interfaceMAC[0] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[0];
	routing_table_entry[rt_table_entry].interfaceMAC[1] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[1];
	routing_table_entry[rt_table_entry].interfaceMAC[2] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[2];
	routing_table_entry[rt_table_entry].interfaceMAC[3] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[3];
	routing_table_entry[rt_table_entry].interfaceMAC[4] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[4];
	routing_table_entry[rt_table_entry].interfaceMAC[5] = (u_int8_t)(if_req.ifr_hwaddr.sa_data)[5];
	rt_table_entry++;

}

int build_routing_table() {
	int type, flags;
	char temp[20];

        FILE *fp = fopen ("/proc/net/arp", "r");
        char ip[128] = { 0x00}, hwa [128] = {0x00}, mask[128] = {0x00}, line[128] = {0x00}, dev[128] =  {0x00};

        if (!fp) 
		return-1;
        fgets (line , sizeof(line) , fp);
        while ( fgets(line, sizeof(line), fp)){
                sscanf(line,"%s    0x%x    0x%x    %s      %s      %s\n", ip , &type, &flags, hwa, mask, dev);
		strcpy(arpContents[arp_entry].ip, ip);
		strcpy(arpContents[arp_entry].ifc, dev);
		strcpy(arpContents[arp_entry].mac, hwa);
		arp_entry++;
		printf("\nEntry : %s %s %s", ip, dev, hwa);
	}
	
	int ret;

	strcpy(temp, "10.99.0.1");
	ret = find_entry_arp_contents(temp);
	if(ret < 0) {
		printf("ARP Content error");
		return 0;
	}		
	insert_rt("10.1.0.0/24" , "10.99.0.1" , arpContents[ret].mac , arpContents[ret].ifc);
	strcpy(temp, "10.99.0.2");
	ret = find_entry_arp_contents(temp);
	if(ret < 0) {
		printf("ARP Content error");
		return 0;
	}		
	insert_rt("10.1.2.0/24" , "10.99.0.2" , arpContents[ret].mac , arpContents[ret].ifc);
	insert_rt("10.1.3.0/24" , "10.99.0.2" , arpContents[ret].mac , arpContents[ret].ifc);

	/*strcpy(temp, "10.10.0.1");
	ret = find_entry_arp_contents(temp);
	if(ret < 0) {
		printf("ARP Content error");
		return 0;
	}		
	insert_rt("10.10.0.0/24" , "10.10.0.1" , arpContents[ret].mac , arpContents[ret].ifc);

	*/
	print_routing_table();
	return 0;
}
