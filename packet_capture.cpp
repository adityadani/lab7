#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "packet_queue.h"

extern int build_routing_table();
extern void * send_on_raw(void *id);
extern int check_my_packet(const u_char *packet, char *args);

pthread_mutex_t queue_mutex; 
char interface1[10], interface2[10];

int intf;
std::queue<QueueEntry> pkt_queue;

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
	QueueEntry entry;
	char *ptr;
	if(intf == 1)
		ptr = interface1;
	else {
		ptr = interface2;
		//return;		
	} 
	
	if(!check_my_packet(packet, ptr)) {
		entry.hdr_len = header->caplen;
		//memcpy(&entry.hdr, header, sizeof(struct pcap_pkthdr));
		entry.pkt = (char *)malloc(header->caplen);
		memcpy(entry.pkt, packet, header->caplen);
		pthread_mutex_lock(&queue_mutex);
		pkt_queue.push(entry);
		pthread_mutex_unlock(&queue_mutex);
		//free(entry.pkt);
		//printf("\nGot the packet with len : %d", header->caplen);
	}
	else {
		//printf("\nDropping the packet");
	}
}

int get_interface_fd(char *dev, pcap_t **handle)
{
	bpf_u_int32 mask;
	bpf_u_int32 net;
	struct bpf_program fp;
	
	char errbuf[PCAP_ERRBUF_SIZE];
	char filter[40];
	if (dev == NULL) {
		fprintf(stderr, "error in finding default device: %s\n", errbuf);
		return(2);
	}
	
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "error in getting netmask for device %s: %s\n", dev, errbuf); // for now, we are not going to use the values in net & mask
		net = 0;
		mask = 0;
	}
	
	*handle = pcap_open_live(dev, BUFSIZ, 0, 1000, errbuf);
	strcpy(filter, "ip and ether dst");
	strcat(filter, dev);
	//pcap_compile(*handle, &fp, filter, 0, mask);
	//pcap_setfilter(*handle, &fp);
	//pcap_setnonblock(*handle, 1, errbuf);
	if (*handle == NULL) {
		fprintf(stderr, "error in opening the device %s: %s\n", dev, errbuf);
		return(2);
	}

	return pcap_get_selectable_fd(*handle);

}
int main(int argc, char *argv[])
{
	pcap_t *handle1, *handle2;			
	char dev[20];	
	struct bpf_program fp;		
	fd_set interface_read_set;
	pthread_t raw_socket_t;
	long t1=1;

	struct pcap_pkthdr header;	
	const u_char *packet;		
	int num_packets=10;
	
	int interface1_fd,interface2_fd;
	//struct timeval tv;
	//tv.tv_sec = 0;
	//tv.tv_usec = 1000000;         // 1000 msec. 
	
	build_routing_table();
	strcpy(interface1, argv[1]);
	strcpy(interface2, argv[2]);
	
	//get pcap session handles and fds for select
	interface1_fd = get_interface_fd(argv[1],&handle1);
	interface2_fd = get_interface_fd(argv[2],&handle2);
	int maxfd = (interface1_fd>interface2_fd)?interface1_fd:interface2_fd;

	pthread_mutex_init(&queue_mutex, NULL);
	pthread_create(&raw_socket_t, NULL, send_on_raw, (void *)t1);

	/* Grab a packet */
	int i=0;
	while(1) {
	FD_ZERO(&interface_read_set);
	FD_SET(interface1_fd, &interface_read_set);
	FD_SET(interface2_fd, &interface_read_set);
	   if(select(maxfd+1,&interface_read_set,NULL,NULL,NULL) > 0){
		if(FD_ISSET(interface1_fd, &interface_read_set)){
			intf = 1;
			pcap_loop(handle1, 1, got_packet, NULL);
		}else if(FD_ISSET(interface2_fd, &interface_read_set)){
			intf = 2;
			pcap_loop(handle2, 1, got_packet, NULL);
		}
	   }
	   i++;
	}
	pthread_join(raw_socket_t, NULL);
	return(0);
}
