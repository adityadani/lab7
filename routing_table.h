using namespace std;

// Build Routing Table (routing_table.cpp)

typedef struct tagArpFileContents {
	char ip[20];
	char mac[20];
	char ifc[20];
} arpFileContents;

typedef struct tagroutingtableentry{
	char subnet[150];
	char IP_next_hop[150];
	u_int8_t MAC_next_hop[6];
	char interface[11];
	u_int8_t interfaceMAC[6];
} routing_table_entry_st;


int build_routing_table();
int get_routing_table_entry(char *);
