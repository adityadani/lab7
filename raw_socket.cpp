#include "header.h"
#include <errno.h>
#include <pthread.h>
#include "packet_queue.h"
#include <netinet/ip_icmp.h>
#include <errno.h>

int tx_len = 0;
char sendbuf[1024];
char source_ip[16], dest_ip[16];
char icmp_packet[65507];
struct sockaddr_ll socket_address_1, socket_address_2;
int socknew_1, socknew_2;

extern char interface1[];
extern  char interface2[];
extern pthread_mutex_t queue_mutex;

// extern functions

// icmp_packet.cpp
extern int create_icmp_packet(char *incoming_packet , char *outgoing_packet , int type , int code);
// longest_prefix_match.cpp
extern int get_routing_table_entry(char *);
// routing_table.cpp
extern int check_arp_for_destination_unreachable(char *ip);

void copy_queue_entry(QueueEntry *src, QueueEntry *copy) {
	//memcpy(&(copy->hdr), &(src->hdr), sizeof(struct pcap_pkthdr));
	copy->hdr_len = src->hdr_len;
	copy->pkt = (char *)malloc(src->hdr_len);
	memcpy(copy->pkt, src->pkt, src->hdr_len);
}

void send_the_packet(char *pkt, int pkt_len, char *interface) {
	if(strcmp(interface, interface1) == 0) {
		/* Send packet */
		if (sendto(socknew_1, pkt, pkt_len, 0, (struct sockaddr*)&socket_address_1, sizeof(struct sockaddr_ll)) < 0)
			printf("Send failed %s\n ", strerror(errno));
		else
			printf("\nYEEEEAAAAAHHHHHHHHHHHHH");
	} else if(strcmp(interface, interface2) == 0) {
		/* Send packet */
		if (sendto(socknew_2, pkt, pkt_len, 0, (struct sockaddr*)&socket_address_2, sizeof(struct sockaddr_ll)) < 0)
			printf("Send failed %s \n", strerror(errno));
		else
			printf("\nYEEEEAAAAAHHHHHHHHHHHHH");			
	}
}

void * send_on_raw(void *id) {

	struct ether_header *eh;
	struct iphdr *iph;
	struct icmphdr *icmph;
	struct ifreq if_mac_1, if_mac_2, if_ip_1, if_ip_2, if_idx_1, if_idx_2;
	QueueEntry entry, entry_copy, *pkt; 
	int ni, ne, nu, n;
	char dest_ip_addr[20], *interface;
	char ip_interface1[20], ip_interface2[20];
	get_mac_address(&if_mac_1, interface1);
	get_ip_address(&if_ip_1, interface1);
	
	get_mac_address(&if_mac_2, interface2);
	get_ip_address(&if_ip_2, interface2);

	get_ip_address_index(&if_idx_1, interface1);
	get_ip_address_index(&if_idx_2, interface2);
	
	printf("Got an IP 1 : %s", inet_ntoa(((struct sockaddr_in *)&if_ip_1.ifr_addr)->sin_addr));
	strcpy(ip_interface1, inet_ntoa(((struct sockaddr_in *)&if_ip_1.ifr_addr)->sin_addr));
	strcpy(ip_interface2, inet_ntoa(((struct sockaddr_in *)&if_ip_2.ifr_addr)->sin_addr));
	/* Destination address */
	/* Index of the network device */
	socket_address_1.sll_ifindex = if_idx_1.ifr_ifindex;
	/* Address length*/
	socket_address_1.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address_1.sll_addr[0] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[0];
	socket_address_1.sll_addr[1] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[1];
	socket_address_1.sll_addr[2] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[2];
	socket_address_1.sll_addr[3] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[3];
	socket_address_1.sll_addr[4] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[4];
	socket_address_1.sll_addr[5] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[5];

	/* Destination address */
	/* Index of the network device */
	socket_address_2.sll_ifindex = if_idx_2.ifr_ifindex;
	/* Address length*/
	socket_address_2.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address_2.sll_addr[0] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[0];
	socket_address_2.sll_addr[1] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[1];
	socket_address_2.sll_addr[2] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[2];
	socket_address_2.sll_addr[3] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[3];
	socket_address_2.sll_addr[4] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[4];
	socket_address_2.sll_addr[5] = (u_int8_t)(if_mac_2.ifr_hwaddr.sa_data)[5];


	ni =  sizeof(struct iphdr);
	ne = sizeof(struct ether_header);
	nu =  sizeof(struct udphdr);
	//printf("IP : %d , Eth : %d UDP : %d",ni, ne, nu);

	//struct udphdr *udph = (struct udphdr *)(iph + sizeof(struct iphdr));
	
	/* Open RAW socket to send on */
	if ((socknew_1 = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror("socket");
	}

	if ((socknew_2 = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror("socket");
	}

	while(1) {
		pthread_mutex_lock(&queue_mutex);
		if(!pkt_queue.empty()) {			
			entry = pkt_queue.front();
			copy_queue_entry(&entry, &entry_copy); // Making a copy of the entry in the queue. So that we can pop it.
			pkt_queue.pop();
			//free(entry.pkt);
			printf("\n Queue Size : %d", pkt_queue.size());
			pthread_mutex_unlock(&queue_mutex);
		}
		else {
			pthread_mutex_unlock(&queue_mutex);
			//usleep(1);
			continue;
		}

		eh = (struct ether_header *)entry_copy.pkt;
		iph = (struct iphdr *)(entry_copy.pkt + sizeof(struct ether_header));
		icmph = (struct icmphdr *)(entry_copy.pkt + sizeof(struct ether_header) + sizeof(struct iphdr));


		//printf("\nPacket from queue : Dest ");
		get_ip_from_int32(iph->daddr, dest_ip_addr);
		int entry, pkt_len;

		if((strcmp(dest_ip_addr,ip_interface1)==0) || (strcmp(dest_ip_addr, ip_interface2)==0)) {
			//Packet is destined to me. No need to search the routing table.
			// We check whether it is an ICMP echo request. If yes then send a reply.
			// Else it might be any other packet destined to me. No need to handle it further.
			// Might need to handle any other packet type for which we need to send a reply!!!!

			if((iph->protocol == 1) && (icmph->type == ICMP_ECHO)) {
							
				// Need to send ICMP echo reply
				
				u_int8_t temp_mac;
				for(int itr = 0 ; itr < 6 ; itr++) {
					temp_mac = eh->ether_dhost[itr];
					eh->ether_dhost[itr] = eh->ether_shost[itr];
					eh->ether_shost[itr] = temp_mac;
				}
				
				u_int32_t temp_ip;
				temp_ip = iph->saddr;
				iph->saddr = iph->daddr;
				iph->daddr = temp_ip;
				iph->check = 0;
				iph->check = csum((unsigned short *)(iph), sizeof(struct iphdr)/2);

				icmph->type = ICMP_ECHOREPLY;
				icmph->code = 0;
				icmph->checksum = csum((unsigned short *)(icmph), (entry_copy.hdr_len - sizeof(struct ether_header) - sizeof(struct iphdr))/2); //can be wrong !!! 
				get_ip_from_int32(iph->daddr, dest_ip_addr);
				entry = get_routing_table_entry(dest_ip_addr);
				interface = get_interface(entry);
				
				send_the_packet(entry_copy.pkt, entry_copy.hdr_len, interface);				
			}
			continue;
		}

		entry = get_routing_table_entry(dest_ip_addr);
		if(entry == -1) {
			// Could not find an entry. Leaving the packet and sending a destination host unreachable message.
			printf("\nCould not find an entry");
			// Check in our ARP table if destination IP is present.
			// If present indicates local IP and hence the kernel will handle it.
			// Else send a destination unreachable.
			if(check_arp_for_destination_unreachable(dest_ip_addr) != -1) {
				continue;
			}
			pkt_len = create_icmp_packet(entry_copy.pkt, icmp_packet, ICMP_DEST_UNREACH, ICMP_HOST_UNREACH); // The code can be different like
															// ICMP_HOST_UNREACH for host unreachable.

			printf("\nDestination Unreachable packet size : %d", pkt_len);
			struct iphdr *tmp_iphdr;
			tmp_iphdr = (struct iphdr *)(icmp_packet + sizeof(struct ether_header));
			get_ip_from_int32(tmp_iphdr->daddr, dest_ip_addr);
			entry = get_routing_table_entry(dest_ip_addr);
			interface = get_interface(entry);
			printf("Sending to %s on interface %s entry : %d", dest_ip_addr, interface, entry);
			send_the_packet(icmp_packet, pkt_len, interface);
		}
		else {
			if(iph->ttl == 1) {
				// Need to send ICMP Time exceeded packet
				pkt_len = create_icmp_packet(entry_copy.pkt, icmp_packet, ICMP_TIME_EXCEEDED, ICMP_EXC_TTL);

				printf("\n Trace route packet len : %d", pkt_len);
				struct iphdr *tmp_iphdr;
				tmp_iphdr = (struct iphdr *)(icmp_packet + sizeof(struct ether_header));
				get_ip_from_int32(tmp_iphdr->daddr, dest_ip_addr);
				entry = get_routing_table_entry(dest_ip_addr);
				interface = get_interface(entry);
				send_the_packet(icmp_packet, pkt_len, interface);
			}
			else {
				/// *************** FORWARDING ******************

				// TTL value is correct and also we found a routing entry. Route the packet correctly.
				// Modify its ethernet and ip headers and forward the packet.
				iph = change_ip_header(iph);		
				interface = change_ethernet_header(eh, entry);
				// This is the packet to be forwarded. Forwarding right away.
				send_the_packet(entry_copy.pkt, entry_copy.hdr_len, interface);

				/// *************** FORWARDING ENDS ****************
			}
		}
	}
	return 0;
}

