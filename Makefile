router_app: header.o raw_socket.o packet_capture.o routing_table.o longest_prefix_match.o icmp_packet.o
	g++ -o router_app -g header.o raw_socket.o packet_capture.o routing_table.o longest_prefix_match.o icmp_packet.o -lpcap -lpthread

header.o: header.h header.cpp
	g++ -g -c -Wall header.cpp

raw_socket.o: header.h raw_socket.cpp packet_queue.h
	g++ -g -c -Wall raw_socket.cpp -lpthread

packet_capture.o: packet_queue.h packet_capture.cpp
	g++ -g -c -Wall packet_capture.cpp  -lpthread -lpcap

routing_table.o: routing_table.cpp routing_table.h
	g++ -g -c -Wall routing_table.cpp

longest_prefix_match.o: longest_prefix_match.cpp 	
	g++ -g -c -Wall longest_prefix_match.cpp

icmp_packet.o: icmp_packet.cpp 	
	g++ -g -c -Wall icmp_packet.cpp

clean:
	rm -rf *.o router_app
