#pragma once
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ether.h>
#include <netinet/ip_icmp.h>
#include <linux/if_packet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// Modifying Header (header.cpp)

extern int tx_len;
extern char sendbuf[1024];
extern char source_ip[16], dest_ip[16];

#define MY_DEST_MAC0	0x00
#define MY_DEST_MAC1	0x00
#define MY_DEST_MAC2	0x00
#define MY_DEST_MAC3	0x00
#define MY_DEST_MAC4	0x00
#define MY_DEST_MAC5	0x05


struct iphdr * change_ip_header(struct iphdr * iph);
void get_mac_address(struct ifreq * if_mac, char *interface);
void get_ip_address(struct ifreq *if_idx, char *interface);
void get_ip_address_index(struct ifreq *if_idx, char *interface);
char * change_ethernet_header(struct ether_header *eh, int entry);
struct udphdr * change_udp_header(struct udphdr *udph);
void get_ip_from_int32(uint32_t ip, char *ip_str);
unsigned short csum(unsigned short *buf, int nwords);
char * get_interface(int);
