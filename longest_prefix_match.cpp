#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include "routing_table.h"

extern routing_table_entry_st routing_table_entry[];
extern int rt_table_entry;


int toDigit(char a){
	switch(a){
	case '0': return 0;
	case '1': return 1;
	case '2': return 2;
	case '3': return 3;
	case '4': return 4;
	case '5': return 5;
	case '6': return 6;
	case '7': return 7;
	case '8': return 8;
	case '9': return 9;
	default : return -1;
	}
}
int pow(int base, int exponent){
	int prod = 1;
	for(int i=1; i <= exponent ; i++){
		prod *= 2;
	}
	return prod;
}

int get_routing_table_entry(char *input_ip_str)
{
	// Take Packet as input 
	int entry =0;
	int value,count=1, flag = 1;
	char *str = (char *)malloc(30);
	char *pIP = (char *)malloc(30);
	char *pp = (char *)malloc(30);
	strcpy(pIP, input_ip_str);
	int maxSub = 0, index = -1;
	char * pch, prev[30], *pktIP,  *subMask, str1[30];
	strcpy(str1,str);
	//printf ("\nSearching route for %s",pIP);
	int start = 0 , routing_entry=0;
	int n = 8, in1=0 , in2 = 0; //edit1
	const char *delim = ".";

  	strcpy(prev, pIP);

	while(routing_entry < rt_table_entry){
		//while there are entries in the routing table
		n = 8;
		flag = 1;
		strcpy(str1,routing_table_entry[routing_entry].subnet);
		subMask = strtok (str1,"/"); 
		strcpy(str,subMask);
	        in1 = 0; //edit
                in2 = 0; //edit
		str[strlen(subMask)] = '\0';
		//printf("STRLEN (STR) = %d" , strlen(str));
		subMask = strtok (NULL,"/");
		//printf ("str = %s\n",str);
		int subM = atoi(subMask);
		strcpy(pp, prev);
		strcpy(pIP, prev);
		pktIP = strtok (pIP,delim);	 
		int sum = 0;
		count = 1;
		start = 0;
	        char pp2[4], str2[4]; //add

		//4 parts of the IP
		while (count <= 4 ){
			int i = 0;
                                        //in = p;
            while(str[in2] != '.' && str[in2] != '\0' ){
                str2[i] = str[in2];                                              
            	in2++;i++;
            }
            in2++;
            str2[i] = '\0';                    
			if(n <= subM){
                                        if(strcmp(pktIP, str2) != 0) {
                                          flag = 0;
                                          break;
                                        }
                                        strcpy(pp2, "");
                                        strcpy(str2, "");
                                }

                                else{

                                                n = n - subM;
                                                //printf(" The value of n is %d \n", n);
                                                int p=0, i=0 , dig;
                                                p = atoi(str2);
                                                if(start != 1){
                                                        while(n <  8){
                                                                sum += pow(2,n);
                                                                //printf("sum inside while loop = %d" , sum);
                                                                n++;
                                                        }
                                                        //printf(" \n sum is %d \n ", (int)sum);
                                                        value = sum & atoi(pktIP);
                                                        if(value != p) {
                                                                flag =0;
                                                                break;
                                                        }
                                                        sum=0;
                                                        p=0;
                                                        start = 1;
                                                }
                                                else{
                                                        if(p != 0){
                                                                flag = 0;
                                                                break;
                                                        }
                                                }
                                        }                                
                                if(flag == 0 ) break;
                                pktIP = strtok (NULL, delim);
                                count++;

                                n = 8 * count;

                         }
		if (flag == 1){
			if(subM > maxSub){
		 		maxSub = subM;
		 		index = routing_entry; // ind is the index for a particular routing table entry
		 	}
		}
	 	routing_entry++;
		//outer while ends here
	}
	//printf("\n Final Index : %d", index);
	free(str);
	free(pIP);
	//free(prev);
	free(pp);
	return index;
}
