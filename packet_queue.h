#include <queue>
#include <pcap.h>

using std::queue;

typedef struct tagQueueEntry {
	int hdr_len;
        char *pkt;
}QueueEntry;

extern std::queue<QueueEntry> pkt_queue;

extern int send_on_raw(char **argv);
